/**
 * MIT License
 * 
 * Copyright (c) 2020 Julian Heng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef WINWAL_H
#define WINWAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "winwal_define.h"
#include "winwal_globals.h"

enum wallpaper_style
{
    STYLE_TILE,
    STYLE_CENTER,
    STYLE_STRETCH,
    STYLE_FIT,
    STYLE_FILL,
    STYLE_NOTSET,
};

typedef struct winwal_app_t
{
	// Stretchy buffer
    char** sources;
    enum wallpaper_style style;
    bool clear_appdata;

    // Return code
    int ret;
} winwal_app_t;

void find_images(const char path[WINWAL_MAX_PATH], char*** file_paths);
void prepare_dest_path(const char path[WINWAL_MAX_PATH],
                       char dest[WINWAL_MAX_PATH]);
bool prepare_wallpaper(const char path[WINWAL_MAX_PATH],
                       const char dest[WINWAL_MAX_PATH]);
bool set_style(enum wallpaper_style style);


#ifdef __cplusplus
}
#endif

#endif /* WINWAL_H */
