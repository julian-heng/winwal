/**
 * MIT License
 * 
 * Copyright (c) 2020 Julian Heng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <windows.h>

#include <initguid.h>
#include <shlguid.h>
#include <wininet.h>
#include <winuser.h>

#include <shlobj.h>

#include <cwchar>

#include "logger.h"
#include "active_desktop.hpp"

static IActiveDesktop* initialize_active_desktop(void);
static bool __set_wallpaper(const wchar_t path[WINWAL_MAX_PATH]);
static void deinitialize_active_desktop(void);


bool set_wallpaper(const char path[WINWAL_MAX_PATH])
{
    wchar_t tmp_dest[WINWAL_MAX_PATH];
    swprintf(tmp_dest, WINWAL_MAX_PATH, L"%hs", path);
    return __set_wallpaper(tmp_dest);
}


static IActiveDesktop*
initialize_active_desktop(void)
{
    IActiveDesktop* pActiveDesktop;
    HRESULT hr;
    LRESULT lr;

    hr = CoInitializeEx(0, COINIT_APARTMENTTHREADED);
    if (hr != S_OK)
    {
        goto init_error;
    }

    lr = SendMessageTimeout(FindWindow("Progman", NULL), 0x52c, NULL, NULL, 0,
                            500, NULL);
    if (! lr)
    {
        goto timeout_error;
    }

    hr = CoCreateInstance(CLSID_ActiveDesktop, NULL, CLSCTX_INPROC_SERVER,
                          IID_IActiveDesktop, (void**)&pActiveDesktop);
    if (hr != S_OK)
    {
        goto create_instance_error;
    }

    return pActiveDesktop;

init_error:
    logger("CoInitializeEx failed.\n");
    return NULL;

timeout_error:
    logger("SendMessageTimeout failed.\n");
    goto error_cleanup;

create_instance_error:
    logger("CoCreateInstance failed.\n");
    goto error_cleanup;

error_cleanup:
    deinitialize_active_desktop();
    return NULL;
}


static bool
__set_wallpaper(const wchar_t path[WINWAL_MAX_PATH])
{
    bool ret = true;
    IActiveDesktop* pActiveDesktop;
    HRESULT hr;

    pActiveDesktop = initialize_active_desktop();
    if (pActiveDesktop == NULL)
    {
        ret = false;
        goto end;
    }

    hr = pActiveDesktop->SetWallpaper(path, 0);
    if (hr != S_OK)
    {
        ret = false;
        logger("SetWallpaper failed.\n");
        goto cleanup;
    }

    hr = pActiveDesktop->ApplyChanges(AD_APPLY_ALL | AD_APPLY_FORCE |
                                      AD_APPLY_BUFFERED_REFRESH);

    if (hr != S_OK)
    {
        ret = false;
        logger("ApplyChanges failed.\n");
    }

cleanup:
    pActiveDesktop->Release();
    deinitialize_active_desktop();

end:
    return ret;
}


static void
deinitialize_active_desktop(void)
{
    CoUninitialize();
}
