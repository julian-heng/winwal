/**
 * MIT License
 * 
 * Copyright (c) 2020 Julian Heng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdbool.h>
#include <stdio.h>
#include <windows.h>

#include "logger.h"
#include "registry.h"

static char* get_reg_key_path(HKEY k_parent, const char* sub_key);

int
write_registry(HKEY k_parent,
               DWORD type,
               const char* sub_key,
               const char* value_name,
               const char* data)
{
    DWORD ret;
    HKEY h_key;

    logger("Writing \"%s\" to \"%s\\%s\"\n",
           data,
           get_reg_key_path(k_parent, sub_key),
           value_name);

    ret = RegOpenKeyEx(k_parent, sub_key, 0, KEY_WRITE, &h_key);
    if (ret != ERROR_SUCCESS)
    {
        logger("Failed to open key \"%s\". Error code: %u\n",
               get_reg_key_path(k_parent, sub_key),
               (unsigned int)GetLastError());

        return false;
    }

    ret = RegSetValueEx(h_key, value_name, 0, type, (void*)data, sizeof(data));

    if (ret == ERROR_SUCCESS)
    {
        logger("Success writing value!\n");
    }
    else
    {
        logger("Failed writing value! Error code: %u\n",
               (unsigned int)GetLastError());
    }

    RegCloseKey(h_key);

    return ret == ERROR_SUCCESS;
}


static char*
get_reg_key_path(HKEY k_parent, const char* sub_key)
{
    static char buf[BUFSIZ];
    char* parent_str;

    if (k_parent == HKEY_CLASSES_ROOT)
    {
        parent_str = "HKEY_CLASSES_ROOT";
    }
    else if (k_parent == HKEY_CURRENT_USER)
    {
        parent_str = "HKEY_CURRENT_USER";
    }
    else if (k_parent == HKEY_LOCAL_MACHINE)
    {
        parent_str = "HKEY_LOCAL_MACHINE";
    }
    else if (k_parent == HKEY_USERS)
    {
        parent_str = "HKEY_USERS";
    }
    else if (k_parent == HKEY_CURRENT_CONFIG)
    {
        parent_str = "HKEY_CURRENT_CONFIG";
    }
    else
    {
        logger("Internal error: k_parent invalid\n");
        return "";
    }

    snprintf(buf, BUFSIZ, "%s\\%s", parent_str, sub_key);
    return buf;
}
