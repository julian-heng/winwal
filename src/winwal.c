/**
 * MIT License
 * 
 * Copyright (c) 2020 Julian Heng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <dirent.h>
#include <getopt.h>
#include <libgen.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <windows.h>

#define STB_IMAGE_IMPLEMENTATION
#include "../lib/stb/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "../lib/stb/stb_image_write.h"
#include "../lib/stb/stretchy_buffer.h"

#include "active_desktop.hpp"
#include "logger.h"
#include "registry.h"
#include "utils.h"

#include "winwal.h"

static void init(char* argv_0);
static bool parse_args(int argc, char** argv, winwal_app_t* app);

const char* WALLPAPER_STYLE_MAP[] =
{
    [STYLE_TILE] = "0",
    [STYLE_CENTER] = "0",
    [STYLE_STRETCH] = "2",
    [STYLE_FIT] = "6",
    [STYLE_FILL] = "10",
};

const char* TILE_WALLPAPER_MAP[] =
{
    [STYLE_TILE] = "1",
    [STYLE_CENTER] = "0",
    [STYLE_STRETCH] = "0",
    [STYLE_FIT] = "0",
    [STYLE_FILL] = "0",
};

char* prog_name;
char appdata_path[WINWAL_MAX_PATH];
char* help = \
    "%s: Recursively finds images and randomly set as wallpaper\n"
    "\n"
    "Usage: %s [options] [directories]\n"
    "\n"
    "Options:\n"
    "\n"
    "    -s [VALUE] The style for the wallpaper to apply\n"
    "               Possible values are: Tile, Center, Stretch, Fit and Fill\n"
    "    -d [VALUE] The directories to search through\n"
    "    -c [VALUE] Delete all files in the AppData folder (%s)\n";

int
main(int argc, char** argv)
{
    init(argv[0]);

    winwal_app_t app =
    {
        .sources = NULL,
        .style = STYLE_NOTSET,
        .clear_appdata = false,
        .ret = 0,
    };

    // Stretchy buffers
    char** file_paths = NULL;
    char* path;

    char dest[WINWAL_MAX_PATH];

    // Check for arguments
    if (argc < 2)
    {
        goto error_no_arguments;
    }

    if (! parse_args(argc, argv, &app))
    {
        goto cleanup;
    }

    // Clearing out the appdata folder
    if (app.clear_appdata)
    {
        logger("Clearing appdata folder\n");
        find_images(appdata_path, &file_paths);
        for (int i = 0; i < sb_count(file_paths); i++)
        {
            logger("Deleting file: \"%s\"\n", file_paths[i]);
            DeleteFileA(file_paths[i]);
        }

        // Exit application
        goto cleanup;
    }

    // Recursively find all images
    for (int i = 0; i < sb_count(app.sources); i++)
    {
        find_images(app.sources[i], &file_paths);
    }

    if (sb_count(file_paths) == 0)
    {
        goto error_no_files;
    }

    // Pick a random image
    srand(time(NULL));
    path = file_paths[rand() % sb_count(file_paths)];
    logger("Selecting \"%s\"\n", path);

    // Prepare the destination path
    memset(dest, 0, WINWAL_MAX_PATH);
    prepare_dest_path(path, dest);
    logger("Destination is \"%s\"\n", dest);

    // Prepare the wallpaper
    prepare_wallpaper(path, dest);

    // Set the wallpaper style
    app.ret = set_style(app.style);
    if (! app.ret)
    {
        goto finish;
    }

    // Set the wallpaper
    logger("Setting wallpaper to \"%s\"\n", dest);

    app.ret = set_wallpaper(dest);

finish:
    logger(app.ret ? "Success!\n" : "Failure!\n");
    goto cleanup;

error_no_arguments:
    logger("Missing arguments\n");
    logger("Exitting...\n");
    app.ret = 1;
    goto exit;

error_no_files:
    logger("Can't find any images\n");
    logger("Exitting...");
    app.ret = 2;
    goto cleanup;

cleanup:
    for (int i = 0; i < sb_count(app.sources); i++)
    {
        free(app.sources[i]);
    }

    // Cleanup
    for (int i = 0; i < sb_count(file_paths); i++)
    {
        free(file_paths[i]);
    }

    sb_free(file_paths);

exit:
    return app.ret;
}


static void
init(char* argv_0)
{
    // Set program name and appdata location
    // prog_name and appdata_path are global variables
    prog_name = basename(argv_0);
    snprintf(appdata_path, WINWAL_MAX_PATH, "%s" WINWAL_APPDATA_PATH,
             getenv("APPDATA"));

    // Create appdata folder if it doesn't exist
    if (! is_dir(appdata_path))
    {
        mkdir(appdata_path);
    }
}


static bool
parse_args(int argc, char** argv, winwal_app_t* app)
{
    int c;

    while ((c = getopt(argc, argv, "s:d:ch")) != -1)
    {
        switch (c)
        {
            case 's':
                if (strcmp(optarg, "Tile") == 0)
                {
                    app->style = STYLE_TILE;
                }
                else if (strcmp(optarg, "Center") == 0)
                {
                    app->style = STYLE_CENTER;
                }
                else if (strcmp(optarg, "Stretch") == 0)
                {
                    app->style = STYLE_STRETCH;
                }
                else if (strcmp(optarg, "Fit") == 0)
                {
                    app->style = STYLE_FIT;
                }
                else if (strcmp(optarg, "Fill") == 0)
                {
                    app->style = STYLE_FILL;
                }
                else
                {
                    logger("Invalid value for style: '%s'\n", optarg);
                    app->ret = 1;
                    return false;
                }
                break;

            case 'd':
                sb_push(app->sources, optarg);
                break;

            case 'c':
                app->clear_appdata = true;
                break;

            case 'h':
                fprintf(stderr, help, prog_name, prog_name, appdata_path);
                app->ret = 0;
                return false;
                break;

            default:
                app->ret = 1;
                return false;
                break;
        }
    }

    // Remaining arguments
    for (int i = optind; i < argc; i++)
    {
        sb_push(app->sources, argv[i]);
    }

    return true;
}


void
find_images(const char path[WINWAL_MAX_PATH], char*** file_paths)
{
    char full_path[WINWAL_MAX_PATH];
    char* buf;
    char* ext;

    struct dirent* de;
    DIR* dr;

    logger("Entering \"%s\"\n", path);
    dr = opendir(path);

    if (dr == NULL)
    {
        logger("Not a valid directory: \"%s\"\n", path);
        return;
    }

    while ((de = readdir(dr)) != NULL)
    {
        if (strncmp(de->d_name, ".", WINWAL_MAX_PATH) != 0 &&
            strncmp(de->d_name, "..", WINWAL_MAX_PATH) != 0)
        {
            snprintf(full_path, WINWAL_MAX_PATH, "%s\\%s", path, de->d_name);

            if (is_dir(full_path))
            {
                find_images(full_path, file_paths);
            }
            else
            {
                ext = get_file_ext(full_path);
    
                if (strncasecmp(ext, "png", 3) == 0 ||
                    strncasecmp(ext, "bmp", 3) == 0 ||
                    strncasecmp(ext, "tga", 3) == 0 ||
                    strncasecmp(ext, "jpg", 3) == 0 ||
                    strncasecmp(ext, "jpeg", 4) == 0)
                {
                    logger("Found image: \"%s\"\n", full_path);
                    buf = calloc(WINWAL_MAX_PATH, sizeof(char));
                    strncpy(buf, full_path, WINWAL_MAX_PATH);
                    sb_push(*file_paths, buf);
                }
            }
        }
    }

    closedir(dr);
}


void
prepare_dest_path(const char path[WINWAL_MAX_PATH],
                  char dest[WINWAL_MAX_PATH])
{
    // Prepare the destination filename
    char dest_fname[WINWAL_MAX_PATH];
    snprintf(dest_fname, WINWAL_MAX_PATH, "%s.jpg",
             get_filename_without_ext(path));

    snprintf(dest, WINWAL_MAX_PATH, "%s\\%s", appdata_path, dest_fname);
}


bool
prepare_wallpaper(const char path[WINWAL_MAX_PATH],
                  const char dest[WINWAL_MAX_PATH])
{
    int ret = false;

    char* ext;
    int x = 0;
    int y = 0;
    int n = 0;
    unsigned char* data;

    // Skip copying/converting if wallpaper already exists
    if (access(dest, F_OK) != -1)
    {
        logger("\"%s\" already exists.\n", dest);
        return true;
    }

    // Check for jpg to just copy instead of converting
    ext = get_file_ext(path);
    if (strncasecmp(ext, "jpg", 3) == 0 ||
        strncasecmp(ext, "jpeg", 4) == 0)
    {
        logger("\"%s\" is already a jpg.\n", path);
        CopyFile(path, dest, false);
    }
    else
    {
        logger("Converting \"%s\" to jpg\n", path);
        data = stbi_load(path, &x, &y, &n, 0);
        ret = stbi_write_jpg(dest, x, y, n, data, 100);
        stbi_image_free(data);
    }

    logger("\"%s\" -> \"%s\"\n", path, dest);

    return ret;
}


bool
set_style(enum wallpaper_style style)
{
    if (style == STYLE_NOTSET)
    {
        logger("Style is not set, not changing existing style\n");
        return true;
    }

    int ret;
    ret = write_registry(HKEY_CURRENT_USER, REG_SZ, "Control Panel\\Desktop",
                         "WallpaperStyle", WALLPAPER_STYLE_MAP[style]);

    if (! ret)
    {
        return ret;
    }

    ret = write_registry(HKEY_CURRENT_USER, REG_SZ, "Control Panel\\Desktop",
                         "TileWallpaper", TILE_WALLPAPER_MAP[style]);

    return ret;
}
