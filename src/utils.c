/**
 * MIT License
 * 
 * Copyright (c) 2020 Julian Heng
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <libgen.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "utils.h"

char*
get_filename_without_ext(const char path[WINWAL_MAX_PATH])
{
    static char buf[WINWAL_MAX_PATH];
    char* basename = strrchr(path, '\\');

    if (basename == NULL)
    {
        return NULL;
    }

    basename++;

    strncpy(buf, basename, WINWAL_MAX_PATH);
    char* extptr = strrchr(buf, '.');

    if (extptr != NULL)
    {
        *extptr = '\0';
    }

    return buf;
}


char*
get_file_ext(const char path[WINWAL_MAX_PATH])
{
    char* extptr = strrchr(path, '.');
    return extptr != NULL && strlen(extptr) > 1 ? extptr + 1 : NULL;
}


bool
is_dir(const char path[WINWAL_MAX_PATH])
{
    struct stat s;
    stat(path, &s);
    return S_ISDIR(s.st_mode);
}
